#!/bin/bash
set -e

if [[ "$(uname -s)" == "Darwin" ]]; then
    export NDK_HOST_TAG="darwin-x86_64"
elif [[ "$(uname -s)" == "Linux" ]]; then
    export NDK_HOST_TAG="linux-x86_64"
else
    echo "Unsupported OS."
    exit
fi
#compiler_path
COMPILER_DIR="$DEVECO_SDK_HOME/default/openharmony/native/llvm/bin"
export PATH="$COMPILER_DIR:$PATH"

export CARGOKIT_MANIFEST_DIR=../rust
export CARGOKIT_BUILD_DIR=./target/
export CARGOKIT_TOOL_TEMP_DIR=./target/rust_tool
export CARGOKIT_LIB_NAME=super_native_extensions
export CARGOKIT_BUILD_MODE=debug
export CARGOKIT_OUTPUT_DIR=./target/out
export CARGOKIT_TARGET_PLATFORMS=ohos

#ohos sysroot
export PKG_CONFIG_SYSROOT_DIR="$DEVECO_SDK_HOME/default/openharmony/native/llvm"

export CC_aarch64_unknown_linux_ohos=$COMPILER_DIR/aarch64-unknown-linux-ohos-clang
export AR_aarch64_unknown_linux_ohos=$COMPILER_DIR/llvm-ar
export CARGO_TARGET_AARCH64_UNKNOWN_LINUX_OHOS_LINKER=$COMPILER_DIR/aarch64-unknown-linux-ohos-clang
export CARGO_TARGET_AARCH64_UNKNOWN_LINUX_OHOS_AR=$COMPILER_DIR/llvm-ar

export CARGO_TARGET_AARCH64_UNKNOWN_LINUX_OHOS_RUSTFLAGS="-L../ohos/cpp/code/build"

./run_rust_tool.sh build_ohos

cd ../ohos/cpp/code
./build.sh
cd -

mkdir -p ../../super_native_extensions/ohos/libs/arm64-v8a/
cp ./target/aarch64-unknown-linux-ohos/debug/libsuper_native_extensions.so ../../super_native_extensions/ohos/libs/arm64-v8a/
cp ../ohos/cpp/code/build/libDragDropHelper.so ../../super_native_extensions/ohos/libs/arm64-v8a/

